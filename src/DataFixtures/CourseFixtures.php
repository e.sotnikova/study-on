<?php

namespace App\DataFixtures;

use App\Entity\Course;
use App\Entity\Lesson;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CourseFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $course1 = new Course();
        $course1->setCode('web-base-level');
        $course1->setName('Веб-вёрстка. Базовый уровень');
        $manager->persist($course1);

        $course2 = new Course();
        $course2->setCode('java-base-level');
        $course2->setName('JavaScript. Базовый уровень');
        $manager->persist($course2);

        $course3 = new Course();
        $course3->setCode('php-base-level');
        $course3->setName('PHP');
        $manager->persist($course3);

        // Course 1
        $lesson1 = new Lesson();
        $lesson1->setCourse($course1);
        $lesson1->setName('Знакомство с курсом');
        $lesson1->setDescription('Вводная информация');
        $lesson1->setNumberLesson(1);
        $manager->persist($lesson1);

        $lesson2 = new Lesson();
        $lesson2->setCourse($course1);
        $lesson2->setName('Базовый HTML.');
        $lesson2->setDescription('Знакомство с HTML');
        $lesson2->setNumberLesson(2);
        $manager->persist($lesson2);

        $lesson3 = new Lesson();
        $lesson3->setCourse($course1);
        $lesson3->setName('Базовый CSS.');
        $lesson3->setDescription('Знакомство с CSS');
        $lesson3->setNumberLesson(3);
        $manager->persist($lesson3);

        // Course 2
        $lesson4 = new Lesson();
        $lesson4->setCourse($course2);
        $lesson4->setName('Введение');
        $lesson4->setDescription('Вводная информация');
        $lesson4->setNumberLesson(1);
        $manager->persist($lesson4);

        $lesson5 = new Lesson();
        $lesson5->setCourse($course2);
        $lesson5->setName('Переменные и работа с числами');
        $lesson5->setDescription('Переменные и работа с числами');
        $lesson5->setNumberLesson(2);
        $manager->persist($lesson5);

        $lesson6 = new Lesson();
        $lesson6->setCourse($course2);
        $lesson6->setName('Строки, boolean и условные операторы');
        $lesson6->setDescription('Строки, boolean и условные операторы');
        $lesson6->setNumberLesson(3);
        $manager->persist($lesson6);

        // Course 3
        $lesson7 = new Lesson();
        $lesson7->setCourse($course3);
        $lesson7->setName('Введение в программирование');
        $lesson7->setDescription('Введение в программирование');
        $lesson7->setNumberLesson(1);
        $manager->persist($lesson7);

        $lesson8 = new Lesson();
        $lesson8->setCourse($course3);
        $lesson8->setName('Структура управления данными. Базовые концепции');
        $lesson8->setDescription('Структура управления данными. Базовые концепции');
        $lesson8->setNumberLesson(2);
        $manager->persist($lesson8);

        $lesson9 = new Lesson();
        $lesson9->setCourse($course3);
        $lesson9->setName('Протокол HTTP/HTTPS');
        $lesson9->setDescription('Протокол HTTP/HTTPS');
        $lesson9->setNumberLesson(3);
        $manager->persist($lesson9);

        $manager->flush();
    }
}
