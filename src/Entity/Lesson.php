<?php

namespace App\Entity;

use App\Repository\LessonRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LessonRepository::class)
 */
class Lesson
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private ?string $name;

    /**
     * @ORM\Column(type="text", nullable=false)
     */
    private ?string $description;

    /**
     * @ORM\Column(type="integer")
     */
    private ?int $numberLesson;

    /**
     * @ORM\ManyToOne(targetEntity=Course::class, inversedBy="lesson")
     * @ORM\JoinColumn(nullable=false)
     */
    private ?Course $course;

    public function __construct()
    {
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getNumberLesson(): ?int
    {
        return $this->numberLesson;
    }

    public function setNumberLesson(int $numberLesson): self
    {
        $this->numberLesson = $numberLesson;

        return $this;
    }

    public function getCourse(): ?Course
    {
        return $this->course;
    }

    public function setCourse(?Course $course): self
    {
        $this->course = $course;

        return $this;
    }

    public function toArray()
    {
        return [
            'id' => $this->getId(),
            'name' => $this->getName(),
            'description' => $this->getDescription(),
            'numberLesson' => $this->getNumberLesson(),
            'course' => $this->getCourse()
        ];
    }
}
