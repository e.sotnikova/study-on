<?php

namespace App\Tests\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Class CourseTest
 * @package App\Tests
 *
 * Проверить для всех GET/POST экшенов контроллеров, что возвращается корректный http-статус
 * В GET-методах проверить, что возвращается то, что ожидается (например, список курсов в нужном количестве, страница курса с правильным количеством уроков и т.д.).
 * Также проверить, что при обращении по несуществующему URL курса/урока и так далее отдается 404
 * Проверить работу форм создания, редактирования и удаления сущностей.
 * Убедиться, что выполняются необходимые валидации и выдаются соответствующие сообщения об ошибках в формах.
 * Также убедиться, что происходит корректная обработка формы при валидных данных.
 * Проверку стоит осуществлять с точки зрения пользователя, т.е. вызывать отправку формы не на конкретный URL, а, например,
 * сначала в тесте перейти на страницу курса, там нажать "Добавить урок", заполнить форму данными и отправить.
 * После отправки формы добавления урока проверить, что произошел редирект на страницу курса и на ней стало на один урок больше.
 */
class CourseTest extends WebTestCase
{
    public function testIndex(): void
    {
        $client = static::createClient();
        $client->request('GET', '/');

        $this->assertResponseIsSuccessful();
        $this->assertSelectorTextContains('h2', 'Give your feedback');
    }

    public function testCoursePage(): void
    {
        $client = static::createClient();
        $crawler = $client->request('GET', '/');

        $this->assertCount(2, $crawler->filter('h4'));


        $this->assertResponseIsSuccessful();
        $this->assertResponseOk();
    }
}
